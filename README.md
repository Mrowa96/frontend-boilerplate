# Frontend Boilerplate

### Features:

- Uses vanilla JS
- Enables newest CSS syntax and features via PostCSS with modules
- Enables newest JavaScript syntax and features via Babel
- Has optimized production build via Terser
- Has many eslint rules which take care of code syntax quality
- Has Prettier support out of the box
- Automatically generates manifest.json along with favicons for all environments

### What to do in the future:

- add serviceworker?

### Additional info:

Prettier should have exact version of packages
