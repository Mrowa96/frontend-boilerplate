module.exports = app => {
  app.cache(true);

  const presets = ['@babel/preset-env'];

  const plugins = [
    [
      '@babel/plugin-transform-destructuring',
      {
        useBuiltIns: true,
      },
    ],
    [
      '@babel/plugin-proposal-class-properties',
      {
        loose: true,
      },
    ],
    [
      '@babel/plugin-proposal-object-rest-spread',
      {
        useBuiltIns: true,
      },
    ],
    [
      '@babel/plugin-proposal-optional-chaining',
      {
        loose: true,
      },
    ],
    '@babel/plugin-transform-runtime',
  ];

  return {
    presets,
    plugins,
  };
};
